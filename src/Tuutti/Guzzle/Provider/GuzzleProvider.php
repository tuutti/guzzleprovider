<?php

namespace Tuutti\Guzzle\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class GuzzleProvider implements ServiceProviderInterface {

  /**
   * @param Container $container
   */
  public function register(Container $container) {
    $container['guzzle.client'] = function($container) {
      return new \GuzzleHttp\Client();
    };
  }
}
